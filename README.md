# Poll.pl

Certain purpose to exist this app is to provide server side data for Android app.

## Run

create file in project directory called `project.json`, inside this file set project env variables:

```json
{
  "DB_URL": "jdbc:postgresql://localhost:5432/<YOUR_DB>",
  "DB_USER": "<YOUR_USERNAME>",
  "DB_PASSWORD": "<YOUR_PASSWORD>"
}
```
execute:
```bash
gradle build run
```