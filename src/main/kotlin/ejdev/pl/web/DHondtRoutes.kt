package ejdev.pl.web

import ejdev.pl.service.dhondt.DhondtService
import ejdev.pl.service.utils.dateTime
import ejdev.pl.service.utils.int
import ejdev.pl.service.utils.long
import ejdev.pl.service.utils.respondEither
import io.ktor.server.application.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

fun Route.dhondtRoutes() {
    val dhondtService by inject<DhondtService>()
    route("dhondt") {
        get("/last10") {
            call.respondEither(dhondtService.getLast10())
        }
        get("/{start}/{end}") {
            dhondtService.getBetween(
                start = dateTime("start") /* 2022-03-30 */,
                end = dateTime("end")
            ).let { call.respondEither(it) }
        }
        get("/page/{page}/{offset}") {
            dhondtService.getPage(
                page = int("page"),
                offset = long("offset")
            ).let { call.respondEither(it) }
        }
    }
}