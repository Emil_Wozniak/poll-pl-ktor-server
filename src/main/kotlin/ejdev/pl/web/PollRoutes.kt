package ejdev.pl.web

import ejdev.pl.model.poll.knownParties
import ejdev.pl.service.poll.PollService
import ejdev.pl.service.utils.dateTime
import ejdev.pl.service.utils.id
import ejdev.pl.service.utils.respondEither
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

fun Route.pollRoutes() {
    val pollService by inject<PollService>()
    route("polls") {
        route("/parties") {
            get("/colors") {
                call.respond(knownParties)
            }
        }
        get("/month") {
            call.respondEither(pollService.getLastMonth())
        }
        get("/{id}") { call.respondEither(pollService.getById(id)) }
        get("/{startDate}/{endDate}") {
            val startDate = dateTime("startDate")
            val endDate = dateTime("endDate")
            call.respondEither(pollService.getBetween(startDate, endDate))
        }
        get("/") { call.respondEither(pollService.getAll()) }
    }
}

