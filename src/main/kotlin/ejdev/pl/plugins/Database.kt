package ejdev.pl.plugins

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import ejdev.pl.plugins.DatabaseFactory.DbStrategy.CREATE
import ejdev.pl.plugins.DatabaseFactory.DbStrategy.DROP_CREATE
import ejdev.pl.repo.PartyTable
import ejdev.pl.repo.PollTable
import ejdev.pl.repo.UserTable
import io.ktor.server.application.*
import io.ktor.server.config.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

@Suppress("unused")
fun Application.configureDatabase(config: HoconApplicationConfig) {
    DatabaseFactory.init(config)
}

object DatabaseFactory {
    fun init(config: HoconApplicationConfig) {
        val dbUrl = config.property("db.jdbcUrl").getString()
        val dbUser = config.property("db.dbUser").getString()
        val dbPassword = config.property("db.dbPassword").getString()
        val dbDriverClass = config.property("db.dbDriver").getString()
        val strategy = config.property("db.strategy").getDbStrategy()

        Database.connect(
            HikariConfig()
                .apply {
                    driverClassName = dbDriverClass
                    jdbcUrl = dbUrl
                    username = dbUser
                    password = dbPassword
                    maximumPoolSize = 3
                    isAutoCommit = false
                    transactionIsolation = "TRANSACTION_REPEATABLE_READ"
                    validate()
                }.let(::HikariDataSource)
        )
        transaction {
            when (strategy) {
                CREATE -> createAll()
                DROP_CREATE -> {
                    dropAll()
                    createAll()
                }
            }
        }
    }

    fun dropAll() =
        SchemaUtils.drop(UserTable, PollTable, PartyTable)

    fun createAll() =
        SchemaUtils.create(UserTable, PollTable, PartyTable)

    enum class DbStrategy {
        DROP_CREATE, CREATE;
    }

    private fun ApplicationConfigValue.getDbStrategy() =
        DbStrategy.valueOf(this.getString())
}


