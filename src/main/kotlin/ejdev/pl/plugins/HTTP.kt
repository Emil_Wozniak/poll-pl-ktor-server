package ejdev.pl.plugins

import io.ktor.http.HttpHeaders.Authorization
import io.ktor.http.HttpMethod.Companion.Delete
import io.ktor.http.HttpMethod.Companion.Options
import io.ktor.http.HttpMethod.Companion.Patch
import io.ktor.http.HttpMethod.Companion.Post
import io.ktor.http.HttpMethod.Companion.Put
import io.ktor.server.application.*
import io.ktor.server.plugins.cors.routing.*

fun Application.configureHTTP() {
    install(CORS) {
        methods += listOf(Post, Options, Put, Delete, Patch)
        allowHeader(Authorization)
        allowHeader("X-Page-number")
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }
}
