package ejdev.pl.plugins

import ejdev.pl.model.error.JWTPrincipalDetailsError
import ejdev.pl.model.error.toProblemDetails
import ejdev.pl.service.security.JwtService
import ejdev.pl.service.users.UserService
import io.ktor.http.HttpStatusCode.Companion.Unauthorized
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.response.*
import mu.KotlinLogging
import org.koin.ktor.ext.inject

private const val CLAIM = "username"
private const val AUTH_JWT = "auth-jwt"

private val logger = KotlinLogging.logger { }

fun Application.configureSecurity() {
    val userService by inject<UserService>()
    val jwtService by inject<JwtService>()

    install(Authentication) {
        jwt(name = AUTH_JWT) {
            realm = AUTH_JWT
            verifier(jwtService.verifier)
            validate { call ->
                call.payload
                    .getClaim(CLAIM)
                    .asString()
                    .let { userService.getUserBy(it) }
                    .orNull()
                    .let { JWTPrincipal(call.payload) }
            }
            challenge { defaultScheme, realm ->
                logger.info { "$defaultScheme $realm" }
                call.respond(Unauthorized, JWTPrincipalDetailsError.toProblemDetails(call.request))
            }
        }
    }
}

