package ejdev.pl.plugins

import com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT
import com.fasterxml.jackson.datatype.joda.JodaModule
import ejdev.pl.model.error.InternalServerError
import ejdev.pl.model.user.LoginRegister
import ejdev.pl.model.user.User
import ejdev.pl.model.user.UserToken
import ejdev.pl.service.scrapper.HtmlExtractionService
import ejdev.pl.service.security.JwtService
import ejdev.pl.service.users.UserService
import ejdev.pl.service.utils.respondEither
import ejdev.pl.web.dhondtRoutes
import ejdev.pl.web.pollRoutes
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.serialization.jackson.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.http.content.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

fun Application.configureRouting() {
    install(StatusPages, StatusPagesConfig::errorHandling)
    install(ContentNegotiation) {
        jackson {
            enable(INDENT_OUTPUT)
            registerModule(JodaModule())
        }
    }

    val userService: UserService by inject()
    val jwtService: JwtService by inject()
    val extractor: HtmlExtractionService by inject()

    routing {
        post("/register") {
            call.receive<User>()
                .let { userService.register(it) }
                .let { call.respondEither(it) }
        }
        post("/login") {
            call.receive<LoginRegister>()
                .run { userService.getUserBy(email) }
                .map { UserToken(jwtService.createToken(it)) }
                .let { call.respondEither(it) }
        }

        route("api") {
            authenticate("auth-jwt") {
                route("/scrap") {
                    get { call.respondEither(extractor.extract()) }
                    get("/store") { call.respondEither(extractor.store()) }
                }
                get("/hello") {
                    call.principal<JWTPrincipal>()
                        .let(::requireNotNull)
                        .run {
                            val username = payload.getClaim("username").asString()
                            val expiresAt = expiresAt?.time?.minus(System.currentTimeMillis())
                            call.respondText("Hello, $username! Token is expired at $expiresAt ms.")
                        }
                }
            }
            pollRoutes()
            dhondtRoutes()
        }
        get("/") {
            call.respondRedirect("/home")
        }
        staticResources("/", "/static")
    }
}

private fun StatusPagesConfig.errorHandling() {
    exception<Throwable> { call, cause ->
        call.respond(InternalServerError(msg = "$cause", status = BadRequest))
    }
}


