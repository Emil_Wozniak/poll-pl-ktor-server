package ejdev.pl.plugins

import ejdev.pl.service.dhondt.DhondtService
import ejdev.pl.service.poll.PollService
import ejdev.pl.service.scrapper.HtmlExtractionService
import ejdev.pl.service.security.JwtService
import ejdev.pl.service.users.UserService
import ejdev.pl.service.utils.ResourceUtil
import io.ktor.server.application.*
import io.ktor.server.config.*
import org.koin.core.module.dsl.createdAtStart
import org.koin.core.module.dsl.singleOf
import org.koin.core.module.dsl.withOptions
import org.koin.dsl.module
import org.koin.ktor.plugin.Koin

fun Application.configureDI() {
    val applicationConfig = this.environment.config
    val appModule = module {
        single { HOCONConfig(applicationConfig) } withOptions { createdAtStart() }
        singleOf(::ResourceUtil) { createdAtStart() }
        singleOf(::HtmlExtractionService) { createdAtStart() }
        singleOf(::UserService) { createdAtStart() }
        singleOf(::PollService) { createdAtStart() }
        singleOf(::DhondtService) { createdAtStart() }
        singleOf(::JwtService) { createdAtStart() }
    }
    install(Koin) {
        modules(appModule)
    }
}

class HOCONConfig(private val config: ApplicationConfig) {
    fun stringProp(name: String) = config.property(name).getString()
    fun getJWT(): JWTProps = JWTProps(
        issuer = stringProp("jwt.issuer"),
        audience = stringProp("jwt.audience"),
        realm = stringProp("jwt.realm"),
        secret = stringProp("jwt.secret"),
    )
}

data class JWTProps(
    val issuer: String,
    val audience: String,
    val realm: String,
    val secret: String
)
