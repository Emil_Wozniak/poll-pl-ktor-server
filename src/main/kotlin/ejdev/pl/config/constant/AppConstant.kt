package ejdev.pl.config.constant

const val ID = "id"
const val AMOUNT = "amount"
const val START = "start"
const val END = "end"

const val ROUTES = "allRoutes"
const val POLL = "poll"
const val POLLS = "polls"
const val PARTY = "party"

const val EMPTY = ""
const val SPACE = " "
const val UNKNOWN = "-"
const val LETTERS = "[a-zA-Z]+"
const val DOUBLE_REGEX = "\\d+\\.\\d+"

const val POLISH_PARLIAMENT_SEATS_AMOUNT = 460

const val UN_ALLOWED_SIZE = "You must choose at least 1 Poll"

const val DHONDT_THRESHOLD = 5.0

const val DEFAULT_LANGUAGE = "pl"

const val INDEX_HTML = "static/index.html"

const val SSL_KEY = "server.ssl.key-store"
const val CONTEXT_PATH = "server.servlet.context-path"
const val ERROR_MSG = "The host name could not be determined, using `localhost` as fallback"
const val LOCAL = "localhost"
const val PORT = "server.port"
const val ROOT = "/"
const val COMPANY = "company"
const val DATE = "date"

const val EWYBORY_SONDAZE = "https://ewybory.eu/sondaze/"