package ejdev.pl.config

import io.ktor.server.config.*
import java.lang.IllegalArgumentException

enum class Environment  {
    DEV, TEST, PROD;
    companion object {
        fun parse(config: HoconApplicationConfig): Environment =
            when(val env = config.property("ktor.env").getString().uppercase()) {
                 "PROD" -> PROD
                 "TEST" -> TEST
                 "DEV" -> DEV
                 else -> throw IllegalArgumentException("Environment $env is not correct!")
             }
    }
    fun isNotTest() = this != TEST
}