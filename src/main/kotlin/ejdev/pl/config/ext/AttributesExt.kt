package ejdev.pl.config.ext

import io.ktor.server.config.*
import io.ktor.util.*

fun Attributes.isTestEnv(config: HoconApplicationConfig): Boolean =
    allKeys.isEmpty() && config.property("ktor.env").toString().uppercase() != "TEST"