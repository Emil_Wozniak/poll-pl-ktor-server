package ejdev.pl.repo

import ejdev.pl.model.user.User
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object UserTable : IntIdTable() {
    val email: Column<String> = varchar("email", 100)
    val password: Column<String> = varchar("password", 100)
    val active: Column<Boolean> = bool("active")
}

class UserDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : EntityClass<Int, UserDAO>(UserTable)

    var email by UserTable.email
    var active by UserTable.active
    var password by UserTable.password
}

fun UserDAO.toUser(): User = User(
    email = email,
    active = active,
    password = password
)

