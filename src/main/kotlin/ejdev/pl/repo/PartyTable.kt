package ejdev.pl.repo

import ejdev.pl.model.poll.Party
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

object PartyTable : LongIdTable() {
    val sequelId: Column<Long> = long("party_id")
        .uniqueIndex("party_id")
        .autoIncrement()
    val score: Column<String> = varchar("score", 10)
    val name: Column<String> = varchar("name", 100)
    val poll: Column<EntityID<Long>> = reference("poll", PollTable)
}

class PartyDAO(id: EntityID<Long>) : LongEntity(id) {
    companion object : EntityClass<Long, PartyDAO>(PartyTable)

    var score by PartyTable.score
    var name by PartyTable.name
    var poll by PollDAO referencedOn PartyTable.poll


}

fun PartyDAO.toParty() = Party(
    id = id.value,
    score = score,
    name = name,
    pollId = poll.id.value
)
