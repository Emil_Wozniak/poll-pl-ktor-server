package ejdev.pl.repo

import ejdev.pl.model.poll.Poll
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object PollTable : LongIdTable() {
    val sequelId: Column<Long> = long("poll_id")
        .uniqueIndex("poll_id")
        .autoIncrement()
    val company: Column<String> = varchar("company", 100)
    val date: Column<String> = varchar("date", 100)
    val dateFrom: Column<DateTime> = datetime("dateFrom")
    val dateTo: Column<DateTime> = datetime("dateTo")
}

class PollDAO(id: EntityID<Long>) : LongEntity(id) {
    companion object : EntityClass<Long, PollDAO>(PollTable)

    var company by PollTable.company
    var date by PollTable.date
    var dateFrom by PollTable.dateFrom
    var dateTo by PollTable.dateTo
}

fun PollDAO.toPoll(parties: List<PartyDAO> ) = Poll(
    id = id.value,
    company = company,
    date = date,
    dateFrom = dateFrom,
    dateTo = dateTo,
    parties = parties.map { it.toParty() }.toMutableList()
)

fun ResultRow.toPoll() = Poll(
    id = this[PollTable.sequelId],
    company = this[PollTable.company],
    date = this[PollTable.date],
    dateFrom = this[PollTable.dateFrom],
    dateTo = this[PollTable.dateTo],
    parties = mutableListOf()
)

