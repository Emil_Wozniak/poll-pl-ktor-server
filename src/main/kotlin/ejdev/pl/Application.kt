package ejdev.pl

import com.typesafe.config.ConfigFactory
import ejdev.pl.config.Environment
import ejdev.pl.config.ext.isTestEnv
import ejdev.pl.plugins.*
import io.ktor.server.application.*
import io.ktor.server.config.*
import io.ktor.util.*

fun main(args: Array<String>): Unit {

    io.ktor.server.cio.EngineMain.main(args)
}

@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
internal fun Application.module() {
    val config = HoconApplicationConfig(ConfigFactory.load())
    val env = Environment.parse(config)
    if (env.isNotTest()) {
        configureDI()
    }
    configureDatabase(config)
    configureSecurity()
    configureRouting()
    configureHTTP()
    configureMonitoring()
}



