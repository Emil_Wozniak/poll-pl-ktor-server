package ejdev.pl.model.error

import ejdev.pl.model.user.User
import io.ktor.http.*
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import org.joda.time.DateTime

internal data class ProblemDetails(
    val type: String,
    val title: String,
    val detail: String,
    val instance: String,
    val status: Int
) {
    constructor(error: Error, request: ApplicationRequest) : this(
        type = "${request.origin.scheme}://${request.origin.remoteHost}:${request.origin.remotePort}${request.origin.uri}",
        title = error.javaClass.simpleName,
        detail = error.message!!,
        instance = request.path(),
        status = error.status.value
    )
}

internal fun Error.toProblemDetails(request: ApplicationRequest) =
    ProblemDetails(
        type = "${request.origin.scheme}://${request.origin.remoteHost}:${request.origin.remotePort}${request.origin.uri}",
        title = this::class.java.simpleName,
        detail = message!!,
        instance = request.path(),
        status = BadRequest.value
    )

sealed class Error(
    val message: String?,
    val status: HttpStatusCode = BadRequest
)

class FileError(msg: String) : Error(msg) {
    constructor(exception: Throwable) : this(
        exception.message ?: exception.stackTraceToString()
    )
}

class InternalServerError(msg: String, status: HttpStatusCode) : Error(msg, status)

class ScapeError(msg: String) : Error(msg)
class DHondtError(msg: String) : Error(msg)

class PollError(msg: String) : Error(msg) {
    fun toThrowable(): Throwable = Exception(message)
}

data object PollNotFound : Error("No Polls found")
data object PollPageNotFound : Error("Page of polls not found")

class PollBetweenNotFound private constructor(
    msg: String,
) : Error(msg) {
    companion object {
        fun between(startDate: DateTime, endDate: DateTime) =
            PollBetweenNotFound("No Polls between $startDate and $endDate")
    }
}

class UserNotFoundError(email: String) : Error("User with email $email not found")
data object NoUsersFoundError : Error("No users found")
data class UserAlreadyExistError(val user: User) : Error("User: ${user.email} already exist")

data object JWTPrincipalDetailsError : Error("Token is not valid or has expired", BadRequest)