package ejdev.pl.model.dhondt

import ejdev.pl.config.constant.DHONDT_THRESHOLD
import ejdev.pl.config.constant.POLISH_PARLIAMENT_SEATS_AMOUNT

/**
 * D'Hondt method
 */
data class DHondt(var results: List<Seat> = mutableListOf()) {

    data class Seat(val name: String, val seats: Int)

    data class Party(val name: String, val percentage: Double)

    /**
     * @author emil.wozniak.591986@gmail.com
     * @since 1.0
     * @param chairs seats in the parlament.
     * @param parties party names and percentage of the votes for the parties.
     * @param threshold minimal value of percentage allowed for the party.
     */
    constructor(
            chairs: Int = POLISH_PARLIAMENT_SEATS_AMOUNT,
            parties: List<Party>,
            threshold: Double = DHONDT_THRESHOLD,
    ) : this() {
        createResult(parties, threshold, chairs)
    }


    private fun createResult(
        parties: List<Party>,
        threshold: Double,
        chairs: Int
    ) {
        parties
            .filter { it.percentage > threshold }
            .let { filtered ->
                val party = filtered.map(Party::name)
                    .toTypedArray()
                val votes = filtered.map(Party::percentage)
                    .toTypedArray()
                IntArray(votes.size).let { allocated ->
                    val table = Array(chairs) { DoubleArray(votes.size) }
                    buildTable(chairs, votes, table)
                    allocateMinisters(chairs, table, votes, allocated)
                    println(allocated)
                    results = party.indices.map { index ->
                        Seat(party[index], allocated[index])
                    }
                }
            }
    }

    /**
     * Build D'Hondt Table
     *
     * @author emil.wozniak.591986@gmail.com
     * @since 1.0
     * @param chairs seats in the parlament.
     * @param votes percentage of the votes for the parties.
     * @param table D'Hondt table
     */
    private fun buildTable(chairs: Int, votes: Array<Double>, table: Array<DoubleArray>) {
        (0 until chairs).forEach { chair ->
            votes.indices.forEach { index ->
                if (chair == 0) table[chair][index] = votes[index]
                else table[chair][index] = table[0][index] / (chair + 1)
            }
        }
    }

    /**
     * Allocate Ministers using D'Hondt
     * iterate through the number of cabinet posts to allocate
     * find the highest value in the D'Hondt Table
     *
     * @author emil.wozniak.591986@gmail.com
     * @since 1.0
     * @param chairs seats in the parlament.
     * @param votes percentage of the votes for the parties.
     * @param table D'Hondt table
     * @param allocated number of current minister
     */
    private fun allocateMinisters(
            chairs: Int,
            table: Array<DoubleArray>,
            votes: Array<Double>,
            allocated: IntArray,
    ) =
        (1 until chairs + 1).forEach { _ ->
            val index = getMaxElement(table, votes.size, chairs)
            allocated[index] = allocated[index] + 1
        }

    /**
     * get the highest value in the 2D table and remove it!
     * returning the row=(party) to which it belongs
     * @param table D'Hondt table.
     * @param size amount of parties.
     * @param chairs seats in the parlament.
     */
    private fun getMaxElement(table: Array<DoubleArray>, size: Int, chairs: Int): Int {
        var maxValue = table[0][0]
        var nextChair = 0
        var nextSize = 0
        (0 until size).forEach { num ->
            (0 until chairs).forEach { chair ->
                if (table[chair][num] > maxValue) {
                    maxValue = table[chair][num]
                    nextChair = chair
                    nextSize = num
                }
            }
        }
        table[nextChair][nextSize] = 0.0 // zero the highest element for next run
        return nextSize
    }

    companion object {
        fun parse(votes: Array<Double>, parties: Array<String>) =
            votes.mapIndexed { index, value -> Party(parties[index], value) }

        fun parse(supports: Array<Support>): List<Party> {
            val parties = supports.map(Support::party)
            return supports
                .map(Support::vote)
                .mapIndexed { index, value ->
                    Party(parties[index], value)
                }
        }
    }

    data class Support(
            val party: String,
            val vote: Double,
    )
}
