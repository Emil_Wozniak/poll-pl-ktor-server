package ejdev.pl.model.scrape

data class EWyboryPage(
    val httpStatusCode: Int,
    val httpStatusMessage: String,
    val sectionPolls: List<List<String>>,
)