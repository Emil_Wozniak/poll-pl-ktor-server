package ejdev.pl.model.poll

data class PartyColor(
    val name: String,
    val color: String
)

val knownParties = listOf(
    PartyColor(
        name = "PIS",
        color = "416F9E"
    ),
    PartyColor(
        name = "KO",
        color = "E56701"
    ),
    PartyColor(
        name = "POLSKA 2050",
        color = "E4A80A"
    ),
    PartyColor(
        name = "KONFEDERACJA",
        color = "27344B"
    ),
    PartyColor(
        name = "LEWICA",
        color = "A81245"
    ),
    PartyColor(
        name = "PSL",
        color = "3DA63D"
    ),
)
