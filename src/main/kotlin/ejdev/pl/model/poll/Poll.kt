package ejdev.pl.model.poll

import org.joda.time.DateTime

data class Poll(
    var company: String,
    var date: String,
    var dateFrom: DateTime? = DateTime(),
    var dateTo: DateTime? = DateTime(),
    var parties: MutableList<Party> = mutableListOf(),
    var id: Long? = null,
)