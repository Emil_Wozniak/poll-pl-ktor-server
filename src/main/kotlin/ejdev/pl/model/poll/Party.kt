package ejdev.pl.model.poll

data class Party(
    var id: Long? = null,
    var score: String,
    var name: String,
    var pollId: Long? = null,
)