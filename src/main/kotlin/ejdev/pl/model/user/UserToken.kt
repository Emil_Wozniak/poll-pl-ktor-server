package ejdev.pl.model.user

data class UserToken(
    val token: String
)