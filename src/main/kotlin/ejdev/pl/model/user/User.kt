package ejdev.pl.model.user

data class User(
    val id: Int? = null,
    val email: String,
    val password: String,
    val active: Boolean = true,
)