package ejdev.pl.service.security

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm.HMAC256
import ejdev.pl.model.user.User
import ejdev.pl.plugins.HOCONConfig
import java.util.*

internal class JwtService(private val hoconConfig: HOCONConfig) {
    private val jwtConfig = hoconConfig.getJWT()

    val verifier: JWTVerifier = jwtConfig.run {
        JWT
            .require(HMAC256(secret))
            .withAudience(audience)
            .withIssuer(issuer)
            .build()
    }

    fun createToken(user: User): String = jwtConfig.run {
        JWT.create()
            .withAudience(audience)
            .withIssuer(issuer)
            .withClaim("username", user.email)
            .withExpiresAt(Date(System.currentTimeMillis() + 60000))
            .sign(HMAC256(secret))
    }
}