package ejdev.pl.service.utils.date

//import java.time.LocalDate
import ejdev.pl.config.constant.EMPTY
import ejdev.pl.config.constant.LETTERS
import ejdev.pl.config.constant.SPACE
import org.joda.time.DateTime
import org.joda.time.LocalDate

const val BETWEEN_DAYS_RANGE = "(\\d{2}-)(\\d{2}\\.\\d{2})"
const val BETWEEN_MONTHS_RANGE = "(\\d{2}\\.\\d{2}-\\d{2}\\.\\d{2})"
const val FULL_DATE = "(\\d{2}\\.\\d{2}\\.\\d+)"
const val DATE_SEPARATOR = "."
const val RANGE_SEPARATOR = "-"

class DateFormatter {

    fun String.toLocalDate(): List<LocalDate?> =
        this.replaceLetters().let { date ->
            when {
                date == "0" -> listOf()
                date.isBlank() -> listOf()
                date.matches(BETWEEN_DAYS_RANGE.toRegex()) -> date.parseDaysRange()
                date.matches(BETWEEN_MONTHS_RANGE.toRegex()) -> date.parseMonthsRange()
                date.matches(FULL_DATE.toRegex()) -> date.parseFullDate()
                else -> date.parseShortDate()
            }
        }

    private fun String.parseFullDate() = with(this) {
        listOf(
            split(DATE_SEPARATOR).let { (day, month, year) ->
                toISODate(day, month, year)
            }
        ).map { LocalDate.parse(it) }
    }

    private fun String.parseDaysRange() = with(this.split(RANGE_SEPARATOR)) {
        get(1).split(DATE_SEPARATOR).let { (day, month) ->
            listOf(toISODate(get(0), month), toISODate(day, month))
                .map { LocalDate.parse(it) }
        }
    }

    private fun String.parseMonthsRange() = with(this.split(RANGE_SEPARATOR)) {
        listOf(
            get(0).split(DATE_SEPARATOR).let { (day, month) ->
                toISODate(day, month)
            },
            get(1).split(DATE_SEPARATOR).let { (day, month) ->
                toISODate(day, month)
            }
        ).map { LocalDate.parse(it) }
    }

    private fun String.parseShortDate() = with(this) {
        listOf(
            split(DATE_SEPARATOR)
                .filter { it.isNotBlank() }
                .map { it.replaceBlack() }
                .let { (day, month) -> toISODate(day, month) }
        ).map { date -> LocalDate.parse(date) }
    }

    private fun toISODate(
        day: String,
        month: String,
        year: String = "${LocalDate.now().year}",
    ) = "$year-$month-$day"
}

private const val INVALID_SYMBOL = "–"

private fun String.dotFormat(): DateTime =
    this.split(".").let { (day, month, year) ->
        "${year}-${month}-${day}".toDateTime()
    }

private fun String.dashFormat(yearIndex: Int, monthIndex: Int): DateTime = run {
        return DateTime.now()
        this.split("\\:(\\s+)?".toRegex())[1].run {

        println("Dash format $this")
        val parts = split("-")
        val dateTo = parts[1].split(".")
        val year = dateTo[yearIndex]
        val month = dateTo[monthIndex]
        return if (!parts[0].contains(".") && parts[0].isNotBlank()) {
            val day = dateTo[0]
            "$year-$month-$day".toDateTime()
        } else {
            parts[0].let {
                println("dot day: $it")
                if (it.contains(".")) it.split(".").let { (day, month) ->
                    "${year}-${month}-${day}".toDateTime()
                } else "${year}-${month}-${it}".toDateTime()
            }
        }
        }
    }

fun String.getDateFrom(): DateTime = with(if (this.contains(INVALID_SYMBOL)) this.replace(INVALID_SYMBOL, "-") else this) {
    println("Date from $this")
    return@with DateTime.now()
    this.split("\\:(\\s+)?".toRegex())[1].run {
        val monthIndex = 1
        val yearIndex = 2
        when {
            contains("-") -> dashFormat(yearIndex, monthIndex)
            contains("[0-9]{2}.[0-2]{2}.[0-9]{4}".toRegex()) -> dotFormat()
            contains(".*[a-zA-Z].*".toRegex()) -> DateTime()
            else -> DateTime()
        }
    }
}

fun String.getDateTo(): DateTime {
    this.replace("$LETTERS.".toRegex(), EMPTY).run {
        println("DateTo $this")
        val receiver = if (this.contains(INVALID_SYMBOL)) this.replace(INVALID_SYMBOL, "-") else this
        val dateTime = with(receiver) {
            when {
                contains("-") -> dashFormat(2, 1)
                contains("[0-9]{2}.[0-2]{2}.[0-9]{4}".toRegex()) -> dotFormat()
                contains(".*[a-zA-Z].*".toRegex()) -> DateTime()
                else -> DateTime()
            }
        }
        return dateTime
    }
}

fun String.toDateTime(): DateTime = DateTime(this)

fun String.replaceLetters() = replace(LETTERS.toRegex(), EMPTY)

fun String.replaceBlack() = this.replace(SPACE, EMPTY)
