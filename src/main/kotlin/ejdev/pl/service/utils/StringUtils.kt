package ejdev.pl.service.utils

import ejdev.pl.config.constant.DOUBLE_REGEX
import ejdev.pl.config.constant.EMPTY
import ejdev.pl.config.constant.LETTERS

fun parseOrReplace(text: String): Double =
    if (text.matches(DOUBLE_REGEX.toRegex())) text.toDouble()
    else tryReplace(text)

private fun tryReplace(text: String): Double =
    text.replace(LETTERS.toRegex(), EMPTY).let { replace ->
        if (replace.matches(DOUBLE_REGEX.toRegex())) replace.toDouble()
        else 0.0
    }


