package ejdev.pl.service.utils

import arrow.core.Either
import ejdev.pl.model.error.Error
import ejdev.pl.model.error.ProblemDetails
import ejdev.pl.service.utils.date.toDateTime
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.util.pipeline.*

internal suspend inline fun <reified T : Any> ApplicationCall.respondEither(either: Either<Error, T>) =
    either.fold({ ProblemDetails(it, this.request) }, { it }).let { content ->
        when (content) {
            is ProblemDetails -> HttpStatusCode.allStatusCodes
                .find { statusCode -> statusCode.value == content.status }
                .let(::requireNotNull)
                .let { status -> this.respond(status, content) }

            else -> this.respond(content)
        }
    }

val PipelineContext<Unit, ApplicationCall>.id: Long
    get() = requireNotNull(call.parameters["id"]).toLong()

fun PipelineContext<Unit, ApplicationCall>.dateTime(name: String) =
    requireNotNull(call.parameters[name]).toDateTime()

fun PipelineContext<Unit, ApplicationCall>.int(name: String) =
    requireNotNull(call.parameters[name]).toInt()

fun PipelineContext<Unit, ApplicationCall>.long(name: String) =
    requireNotNull(call.parameters[name]).toLong()