package ejdev.pl.service.utils

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import arrow.core.getOrElse
import ejdev.pl.plugins.HOCONConfig

class ResourceUtil(private val hocon: HOCONConfig) {
    private val resource: Either<NotFound, String> =
        this::class.java.getResource("/")
            ?.path
            ?.let(::Right)
            ?: Left(NotFound)

    val resourcePath: String =
        hocon.stringProp("user.home").let { home ->
            resource
                .map { it.replace("build/classes/kotlin/main", "src/main/resources") }
                .getOrElse { "$home/Pobrane/resources" }
        }
}


object NotFound : Exception("Resource not found")