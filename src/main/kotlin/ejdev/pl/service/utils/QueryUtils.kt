package ejdev.pl.service.utils

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.computations.ensureNotNull
import ejdev.pl.model.error.Error
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

suspend fun <T> eitherEntity(
    error: Error,
    resolve: () -> List<T>
): Either<Error, T> =
    either {
        val value = tryQuery(resolve)
        ensureNotNull(value) { error }
        ensure(value.isNotEmpty()) { error }
        value.first()
    }.mapLeft { it }

suspend fun <T> entities(
    onFailure: Throwable,
    resolve: () -> List<T>
): Either<Throwable, List<T>> =
    either<Throwable, List<T>> {
        val value = tryQuery(resolve)
        ensureNotNull(value) { throw onFailure }
        ensure(value.isNotEmpty()) { throw onFailure }
        value
    }.mapLeft { throw it }

suspend fun <T> entities(
    onFailure: Error,
    resolve: () -> List<T>
): Either<Error, List<T>> =
    either {
        val value = tryQuery(resolve)

        ensureNotNull(value) { onFailure }
        ensure(value.isNotEmpty()) { onFailure }
        value
    }.mapLeft { it }

suspend fun <T> save(error: Throwable, resolve: () -> T): Either<Throwable, T> =
    either<Throwable, T> {
        ensureNotNull(tryFetchOne(resolve)) { throw error }
    }.mapLeft { throw it }

suspend fun <T> dbQuery(block: suspend () -> T): T =
    withContext(IO) {
        newSuspendedTransaction { block() }
    }

fun <
        ID_T : Comparable<ID_T>,
        ID_C : Comparable<ID_C>,
        PARENT_DAO : Entity<ID_T>,
        CHILD_DAO : Entity<ID_C>,
        T,
        > SizedIterable<PARENT_DAO>.eager(
    child: PARENT_DAO.() -> SizedIterable<CHILD_DAO>,
    transform: (PARENT_DAO, List<CHILD_DAO>) -> T
): List<T> = this.map {
    transform(it, child(it).toList())
}

suspend fun <T> tryFetchOne(resolve: () -> T): T? =
    try {
        dbQuery { resolve() }
    } catch (e: Exception) {
        nothing(e)
    }

suspend fun <T> tryQuery(resolve: () -> List<T>): List<T>? =
    try {
        dbQuery { resolve() }
    } catch (e: Exception) {
        nothing(e)
    }

private fun nothing(e: Exception): Nothing? = with(e) {
    println(e.message)
    null
}

class RequestPage<ID : Comparable<ID>, T : Entity<ID>> private constructor(private val table: EntityClass<ID, T>) {
    infix fun get(page: Page): SizedIterable<T> = table.all().limit(page.n, page.offset)

    companion object {
        infix fun <ID : Comparable<ID>, T : Entity<ID>> entity(table: EntityClass<ID, T>) =
            RequestPage(table)
    }
}

data class Page(val n: Int, val offset: Long = 10)
