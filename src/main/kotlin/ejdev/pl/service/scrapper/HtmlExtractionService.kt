package ejdev.pl.service.scrapper

import arrow.core.Either
import arrow.core.computations.either
import ejdev.pl.model.error.Error
import ejdev.pl.model.error.FileError
import ejdev.pl.model.error.ScapeError
import ejdev.pl.model.poll.Party
import ejdev.pl.model.poll.Poll
import ejdev.pl.model.scrape.EWyboryPage
import ejdev.pl.plugins.HOCONConfig
import ejdev.pl.service.poll.PollService
import ejdev.pl.service.utils.ResourceUtil
import ejdev.pl.service.utils.date.getDateFrom
import ejdev.pl.service.utils.date.getDateTo
import it.skrape.core.document
import it.skrape.fetcher.BrowserFetcher
import it.skrape.fetcher.Result
import it.skrape.fetcher.response
import it.skrape.fetcher.skrape
import it.skrape.selects.DocElement
import it.skrape.selects.html5.div
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import mu.KotlinLogging
import java.io.File
import java.time.LocalDateTime

private const val DELIMITER = ";"
private const val URL = "https://ewybory.eu/sondaze/"

private val logger = KotlinLogging.logger {  }

class HtmlExtractionService(
    private val hocon: HOCONConfig,
    private val resourceUtil: ResourceUtil,
    private val pollService: PollService,
) {
    suspend fun store(): Either<FileError, List<List<String>>> =
        either {
            val pathname = "${resourceUtil.resourcePath}/${hocon.stringProp("resource.csv")}"
            File(pathname)
                .apply { ensure(exists()) { FileError("File $pathname does not exist") } }
                .readLines()
                .apply { ensure(isNotEmpty()) { FileError("File $pathname is empty") } }
                .map { it.split(DELIMITER) }
                .also { lines ->
                    runCatching {
                        val parties = lines[0].drop(1)
                        lines
                            .drop(1)
                            .mapNotNull { cols ->
                                if (cols.size >= 7) {
                                    cols.toPoll(parties)
                                } else null
                            }
                            .filter { it.date.isNotBlank() }
                            .also { pollService.saveAll(it) }
                    }
                        .getOrElse(::FileError)
                }
        }

    private fun List<String>.toPoll(parties: List<String>): Poll {
        val (company, date) = this[0].split(":".toRegex())
        return Poll(
            company = company,
            date = date,
            dateFrom = date.getDateFrom(),
            dateTo = date.getDateTo(),
            parties = parties
                .mapIndexed { id, partyName ->
                    Party(
                        score = this[id + 1].replace("[a-zA-Z]+".toRegex(), ""),
                        name = partyName,
                    )
                }
                .toMutableList()
        )
    }


    suspend fun extract(): Either<Error, List<List<String>>> =
        withContext(IO) {
            val resourcePath = resourceUtil.resourcePath
            skrape(BrowserFetcher) {
                request {
                    timeout = 10_000_000
                    url = URL
                }
                response { toEWyboryPage() }
            }.sectionPolls.let { lines ->
                logger.info { "Received from $URL\n" }
                logger.info { "$lines" }
                val date = LocalDateTime.now().let { "${it.year}-${it.month.value}-${it.dayOfMonth}" }
                val pathname = "${resourcePath}scrap-${date}.csv"
                either {
                    ensure(lines.isNotEmpty()) { ScapeError("Scraped data is empty") }
                    println("Save backup to $pathname")
                    File(pathname).apply {
                        createNewFile()
                        ensure(exists()) { ScapeError("File create problem") }
                        writeText(lines.joinToString("\n") { it.joinToString(DELIMITER) })
                    }
                    lines
                }
            }
        }

    private fun Result.toEWyboryPage() =
        EWyboryPage(
            httpStatusCode = status { code },
            httpStatusMessage = status { message },
            sectionPolls = document.div {
                findAll {
                    flatMap { docElement ->
                        docElement.children {
                            filter { element -> element.classNames.contains("section_polls_row") }
                                .map { rows ->
                                    rows.children { map(DocElement::text) }
                                }
                        }
                    }.filter(List<String>::isNotEmpty)
                }
            },
        )
}
