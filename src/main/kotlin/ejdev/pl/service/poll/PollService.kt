package ejdev.pl.service.poll

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.getOrHandle
import ejdev.pl.model.error.*
import ejdev.pl.model.poll.Poll
import ejdev.pl.repo.*
import ejdev.pl.service.utils.*
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.and
import org.joda.time.DateTime
import org.joda.time.DateTime.now

class PollService {
    suspend fun getById(id: Long): Either<Error, Poll> =
        eitherEntity(error = PollError("Poll with id $id was not found")) {
            PollDAO.find { PollTable.id eq id }
                .map {
                    it.toPoll(PartyDAO.find { PartyTable.poll eq it.id }
                        .toList())
                }
        }

    suspend fun getAll(): Either<Error, List<Poll>> =
        entities(onFailure = PollNotFound) {
            PollDAO.all()
                .map {
                    it.toPoll(PartyDAO.find { PartyTable.poll eq it.id }
                        .toList())
                }
        }

    suspend fun saveAll(polls: List<Poll>): Either<Throwable, List<PollDAO>> =
        either {
            polls
                .map { save(it) }
                .map { either -> either.getOrHandle { throw it } }
        }

    private suspend fun save(entity: Poll): Either<Throwable, PollDAO> =
        save(PollError("Save failed").toThrowable()) {
            PollDAO
                .new {
                    company = entity.company
                    date = entity.date
                    dateFrom = entity.dateFrom ?: now()
                    dateTo = entity.dateTo ?: now()
                }
                .also { dao ->
                    entity.parties.forEach { party ->
                        PartyDAO.new {
                            name = party.name
                            score = party.score
                            poll = dao
                        }
                    }
                }
        }

    suspend fun getBetween(startDate: DateTime, endDate: DateTime): Either<DHondtError, List<Poll>> =
        entities(onFailure = PollBetweenNotFound.between(startDate, endDate)) {
            PollDAO
                .find { (PollTable.dateFrom greaterEq startDate) and (PollTable.dateTo lessEq endDate) }
                .map {
                    it.toPoll(PartyDAO.find { PartyTable.poll eq it.id }.toList())
                }
        }.mapLeft {
            DHondtError(it.message!!)
        }

    suspend fun getLastMonth(): Either<Error, List<Poll>> =
        entities(onFailure = PollNotFound) {
            PollDAO
                .find {
                    val minusMonths = DateTime(2022, 12, 1, 8, 0)
                    PollTable.dateFrom less minusMonths
                }
                .orderBy(PollTable.id to SortOrder.DESC_NULLS_LAST)
                .orderBy(PollTable.dateFrom to SortOrder.DESC_NULLS_LAST)
                .map {
                    it.toPoll(PartyDAO.find { PartyTable.poll eq it.id }
                        .toList())
                }
        }

    suspend fun getPollsPage(page: Page) =
        entities(onFailure = PollPageNotFound) {
            (RequestPage entity PollDAO get page).eager()
        }

    private fun SizedIterable<PollDAO>.eager() =
        this.map {
            it.toPoll(PartyDAO.find { PartyTable.poll eq it.id }
                .toList())
        }
}

