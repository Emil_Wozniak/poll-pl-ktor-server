package ejdev.pl.service.dhondt

import arrow.core.Either
import ejdev.pl.config.constant.DHONDT_THRESHOLD
import ejdev.pl.model.dhondt.DHondt
import ejdev.pl.model.dhondt.DHondt.Seat
import ejdev.pl.model.error.Error
import ejdev.pl.model.poll.Poll
import ejdev.pl.service.poll.PollService
import ejdev.pl.service.utils.Page
import org.joda.time.DateTime

interface DHondtApi {
    fun getOne(chairs: Int, votes: Array<Double>, party: Array<String>): List<Seat>

    suspend fun getPage(page: Int, offset: Long): Either<Error, List<Seat>>

    suspend fun getLast10(): Either<Error, List<Seat>>

    suspend fun getBetween(start: DateTime, end: DateTime): Either<Error, List<Seat>>

    suspend fun seats(polls: List<Poll>): List<Seat>

}

internal class DhondtService(
        private val pollService: PollService,
) : DHondtApi {

    override fun getOne(chairs: Int, votes: Array<Double>, party: Array<String>): List<Seat> =
        DHondt(chairs, DHondt.parse(votes, party), DHONDT_THRESHOLD)
            .results

    private val page = Page(10, 1)

    override suspend fun getPage(page: Int, offset: Long): Either<Error, List<Seat>> =
        getPage(Page(page, offset))

    override suspend fun getLast10(): Either<Error, List<Seat>> =
        getPage(Page(10, 0))

    override suspend fun getBetween(start: DateTime, end: DateTime): Either<Error, List<Seat>> =
        pollService.getBetween(start, end)
            .map(::getSupport)
            .map(DHondt.Companion::parse)
            .map { DHondt(parties = it) }
            .map(DHondt::results)

    override suspend fun seats(polls: List<Poll>): List<Seat> = standardDHondt(polls)

    private fun standardDHondt(polls: List<Poll>): List<Seat> =
        DHondt(parties = DHondt.parse(getSupport(polls))).results

    private suspend fun getPage(page: Page): Either<Error, List<Seat>> =
        this.pollService.getPollsPage(page).map(::standardDHondt)

    private fun getSupport(polls: List<Poll>): Array<DHondt.Support> =
        with(hashMapOf<String, MutableList<Double>>()) {
            polls.flatMap(Poll::parties)
                .map { it.name to mutableListOf<Double>() }
                .forEach { (party, scores) -> this[party] = scores }
            polls.forEach { poll ->
                poll.parties
                    .filter { (_, _, name) -> this.containsKey(name) }
                    .forEach { (_, score, name) -> this[name]?.add(score.toDoubleOrDefault()) }
            }
            this
                // party to score
                .map { (party, scores) -> party to scores.sum() / polls.size - 1 }
                .map { (party, score) -> DHondt.Support(party, score) }
                .toTypedArray()
        }

    private fun String.toDoubleOrDefault(): Double =
        if (this.contains("-")) 0.0
        else this.toDouble()

}