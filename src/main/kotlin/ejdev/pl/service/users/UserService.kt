package ejdev.pl.service.users

import arrow.core.Either
import arrow.core.computations.either
import ejdev.pl.model.error.UserAlreadyExistError
import ejdev.pl.model.error.Error
import ejdev.pl.model.error.NoUsersFoundError
import ejdev.pl.model.error.UserNotFoundError
import ejdev.pl.model.user.User
import ejdev.pl.repo.UserDAO
import ejdev.pl.repo.UserTable
import ejdev.pl.repo.toUser
import ejdev.pl.service.utils.eitherEntity
import ejdev.pl.service.utils.entities
import ejdev.pl.service.utils.tryFetchOne

internal class UserService {

    suspend fun getAllUsers(): Either<Error, List<User>> =
        entities(onFailure = NoUsersFoundError) {
            UserDAO.all().map(UserDAO::toUser)
        }

    suspend fun register(user: User): Either<Error, User> =
        either {
            ensure(
                value = tryFetchOne { UserDAO.all().find { it.email == user.email } } == null
            ) { UserAlreadyExistError(user) }
            tryFetchOne {
                UserDAO.new {
                    email = user.email
                    password = user.password
                    active = user.active
                }
            }.let(::requireNotNull)
                .toUser()
        }

    suspend fun getUserBy(email: String): Either<Error, User> {
        return eitherEntity(error = UserNotFoundError(email)) {
            UserDAO
                .find { UserTable.email eq email }
                .map(UserDAO::toUser)
        }
    }
}
