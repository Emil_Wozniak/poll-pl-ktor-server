let url = '';
let login = '';
// noinspection JSUnusedGlobalSymbols
const onChange = (value) => login = value;

window.addEventListener("load", () => {
  const sendData = () => {
    const request = new XMLHttpRequest();
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
      if (request.status === 200) {
        const token = JSON.parse(request.responseText)["token"];
        localStorage.setItem("token", token)
        alert(`Your token:\n ${token}`)
      } else {
        console.error(request.response);
      }
    };
    request.send(JSON.stringify({"email": login}));
  };
  const form = document.getElementById("login_form");
  form.addEventListener("submit", (event) => {
    event.preventDefault();
    sendData();
  });
});