package ejdev.pl

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.koin.test.KoinTest

interface BaseSpec : KoinTest {
    @DslMarker
    @Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY)
    annotation class SpecDsl

    @SpecDsl
    var ApplicationTestBuilder.given: String
        get() = ""
        set(value) {
            value
        }


    @SpecDsl
    var ApplicationTestBuilder.`when`: String
        get() = ""
        set(value) {
            value
        }

    @SpecDsl
    var ApplicationTestBuilder.and: String
        get() = ""
        set(value) {
            value
        }

    @SpecDsl
    var ApplicationTestBuilder.then: String
        get() = ""
        set(value) {
            value
        }

    @SpecDsl
    suspend infix fun HttpResponse.check(validation: suspend HttpResponse.() -> Unit) =
        validation(this)


    val HttpResponse.contentType: String
        get() = contentType()!!.contentType


    companion object {
        val mapper: ObjectMapper  = jacksonObjectMapper()

        suspend inline fun <reified T> HttpResponse.bodyOf() = mapper.readValue(bodyAsText(), T::class.java)

        const val LOGIN_JSON = """{"email": "john.snow@ejdev.pl"}"""
        const val REGISTER_JSON = """
            {
                "email": "john.snow@ejdev.pl",
                "password": "1234",
                "active": true
            }
        """
        const val JWT_TOKEN_JSON_PATTERN = "\\{\\s+\\\"token\\\"\\s+:\\s+\\\"[\\w\\.\\-\\_]+\\\"\\s+\\}"
    }
}