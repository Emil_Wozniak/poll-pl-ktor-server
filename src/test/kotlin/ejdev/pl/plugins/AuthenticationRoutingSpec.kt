package ejdev.pl.plugins

import ejdev.pl.BaseSpec
import ejdev.pl.BaseSpec.Companion.JWT_TOKEN_JSON_PATTERN
import ejdev.pl.BaseSpec.Companion.LOGIN_JSON
import ejdev.pl.BaseSpec.Companion.REGISTER_JSON
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.http.ContentType.*
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.server.testing.*
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.test.AfterTest
import kotlin.test.Test

class AuthenticationRoutingSpec : BaseSpec {

    @Test
    fun `cannot login on not registered email`() = testApplication {
        given = "all modules are installed"
        application {}

        `when` = "send post login request"
        val response = client.post("/login") {
            contentType(Application.Json)
            setBody(LOGIN_JSON)
        }

        then
        with(response) {
            bodyAsText() shouldBe USER_NOT_FOUND_RESPONSE
            contentType shouldBe Application.Json.contentType
            status shouldBe BadRequest
        }
    }

    @Test
    fun `can register email`() = testApplication {
        given = "all modules are installed"
        application {}

        `when` = "send post register request"
        val response = client.post("/register") {
            contentType(Application.Json)
            setBody(REGISTER_JSON)
        }

        then + ""
        with(response) {
            bodyAsText() shouldBe USER_REGISTER_RESPONSE
            contentType shouldBe Application.Json.contentType
            status shouldBe OK
        }
    }

    @Test
    fun `can login on registered email`() = testApplication {
        given = "all modules are installed"
        application {}

        `when` = "send post register request"
        var response = client.post("/register") {
            contentType(Application.Json)
            setBody(REGISTER_JSON)
        }

        then
        with(response) {
            bodyAsText() shouldBe USER_REGISTER_RESPONSE
            contentType shouldBe Application.Json.contentType
            status shouldBe OK
        }

        and = "send post login request"
        response = client.post("/login") {
            contentType(Application.Json)
            setBody(LOGIN_JSON)
        }

        and = ""
        with(response) {
            bodyAsText() shouldContain JWT_TOKEN_JSON_PATTERN.toRegex()
            contentType shouldBe Application.Json.contentType
            status shouldBe OK
        }
    }

    @AfterTest
    fun dropDb() {
        transaction {
            DatabaseFactory.dropAll()
            DatabaseFactory.createAll()
        }
    }

    companion object {
        internal const val USER_REGISTER_RESPONSE = """{
  "id" : null,
  "email" : "john.snow@ejdev.pl",
  "password" : "1234",
  "active" : true
}"""
        private const val USER_NOT_FOUND_RESPONSE = """{
  "type" : "http://localhost:0/login",
  "title" : "UserNotFoundError",
  "detail" : "User with email john.snow@ejdev.pl not found",
  "instance" : "/login",
  "status" : 400
}"""
    }
}