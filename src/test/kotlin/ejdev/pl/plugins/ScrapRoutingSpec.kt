package ejdev.pl.plugins

import com.typesafe.config.ConfigFactory
import ejdev.pl.BaseSpec
import ejdev.pl.BaseSpec.Companion.LOGIN_JSON
import ejdev.pl.BaseSpec.Companion.bodyOf
import ejdev.pl.model.user.UserToken
import ejdev.pl.plugins.AuthenticationRoutingSpec.Companion.USER_REGISTER_RESPONSE
import ejdev.pl.service.dhondt.DhondtService
import ejdev.pl.service.poll.PollService
import ejdev.pl.service.scrapper.HtmlExtractionService
import ejdev.pl.service.security.JwtService
import ejdev.pl.service.users.UserService
import ejdev.pl.service.utils.ResourceUtil
import io.kotest.matchers.shouldBe
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.http.ContentType.*
import io.ktor.http.ContentType.Application
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.HttpStatusCode.Companion.Unauthorized
import io.ktor.server.application.*
import io.ktor.server.config.*
import io.ktor.server.testing.*
import io.mockk.mockk
import org.junit.Rule
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.createdAtStart
import org.koin.core.module.dsl.singleOf
import org.koin.core.module.dsl.withOptions
import org.koin.dsl.module
import org.koin.test.KoinTestRule
import org.koin.test.mock.MockProviderRule
import kotlin.test.Test

class ScrapRoutingSpec : BaseSpec {
    private val config = HoconApplicationConfig(ConfigFactory.load())
    private val htmlExtractionService = mockk<HtmlExtractionService>()

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        mockk(clazz.java.simpleName)
    }

    val modules = module {
        single { HOCONConfig(config) } withOptions { createdAtStart() }
        singleOf(::ResourceUtil) { createdAtStart() }
        single { htmlExtractionService } withOptions { createdAtStart() }
        singleOf(::UserService) { createdAtStart() }
        singleOf(::PollService) { createdAtStart() }
        singleOf(::DhondtService) { createdAtStart() }
        singleOf(::JwtService) { createdAtStart() }
    }

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger()
        modules(
            modules
        )
    }

    @Test
    fun `scrap api is not open only for not authorized user`() = testApplication {
        given = "mocked services"
        application {
            install(createApplicationPlugin(name = "Koin") {
                startKoin {
                    modules(modules)
                }
            }) {

            }
        }

        `when` = "call scrap endpoint"
        val response = client.get("/api/scrap")

        then = "status is 401"
        with(response) {
            status shouldBe Unauthorized
        }
    }

    @Test
    fun `scrap api is open only for authorized users`() = testApplication {
        given = "mocked services"
        application {
            install(createApplicationPlugin(name = "Koin") {
                startKoin {
                    modules(modules)
                }
            }) {

            }
        }

        var response = authorize()
        val token = response.bodyOf<UserToken>().token

        `when` = "call scrap endpoint"
        response = client.get("/api/scrap") {
            header("Authorization", "Bearer $token")
            accept(Application.Json)
        }

        then = "status is 200"
        with(response) {
            status shouldBe OK
            bodyAsText() shouldBe ""
        }
    }

    private suspend fun ApplicationTestBuilder.authorize(): HttpResponse {
        `when` = "send post register request"
        var response = client.post("/register") {
            contentType(Application.Json)
            setBody(BaseSpec.REGISTER_JSON)
        }

        then
        with(response) {
            bodyAsText() shouldBe USER_REGISTER_RESPONSE
            contentType shouldBe Application.Json.contentType
            status shouldBe OK
        }

        and = "send post login request"
        response = client.post("/login") {
            contentType(Application.Json)
            setBody(LOGIN_JSON)
        }
        return response
    }

    private val html = """
        <?xml version="1.0" encoding="UTF-8"?>
        <html lang="pl-PL" prefix="og: https://ogp.me/ns#">
          <head>
          </head>
          <body data-cmplz="1" class="page-template-default page page-id-3525 wp-embed-responsive cs-page-layout-disabled header-large navbar-width-boxed navbar-alignment-left sticky-sidebar-enabled stick-to-bottom block-page-align-enabled">
          </body>
        </html>

    """.trimIndent()
}