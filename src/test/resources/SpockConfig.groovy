//import groovy.json.JsonSlurper
//
////noinspection GrUnresolvedAccess
//runner {
//    println '=> Init Spock configs'
//    filterStackTrace false
//    setReport()
////    setDatabase()
//}
//
//private void setDatabase() {
//    println "=> Set database"
//    final config = new JsonSlurper().parse(new File('project.json'))
//    prop config, 'DB_URL'
//    prop config, 'DB_USER'
//    prop config, 'DB_PASSWORD'
//}
//
//@SuppressWarnings(['GrUnresolvedAccess', 'GroovyGStringKey'])
//private void setReport() {
//    final root = 'com.athaydes.spockframework.report'
//    println '=> Set report'
//    final configs = [
//            "${root}.showCodeBlocks"                             : false,
//            "${root}.outputDir"                                  : 'build/spock/reports',
//            "${root}.internal.HtmlReportCreator.featureReportCss": 'templates/spock/feature-report.css',
//            "${root}.internal.HtmlReportCreator.summaryReportCss": 'templates/spock/summary-report.css'
//    ].tap { Map config -> config.each { k, v -> println "==> $k: $v" } }
//    spockReports { set(configs) }
//}
//
//@SuppressWarnings('GroovyAssignabilityCheck')
//protected static def prop(Object source, String key) {
//    source[key].with {
//        println "==> $key: $it"
//        System.setProperty key, it
//    }
//}